// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
export const BaseURL = "http://3.142.68.27:4000/"
export const Back = "Back";
export const AddBookTitle = "Add Book"
export const BookName="Book Name";
export const PdfTitle = "Pdf File";
export const ImageUrlTitle= 'Image File'
export const BookTypeTitle  = "Book Type"
export const bookNameErrorMsg = "Book Name is Required"
export const PdfErrorMsg = "Pdf File is Required"
export const ImageUrlErrorMsg = "Imaage Url is Required"
export const BookTypeErrorMsg= "Book Type is Required"
export const SubmitButton = "Submit"
export const FileSize="File size above 3mb please compress the pdf"
export const AdminDashboardTitle="Admin Dashboard"
export const Logout = "Logout"
export const HomeName = "Book Library"
export const AllBooksTitle = "All Books"
export const BooksRequests="Book Requests"
export const BooksTitle = "Books"
export const ReadButtonName="Read"
export const UpdateButtonName="Update"
export const deleteButtonname="Delete"
export  const EmptyListName = "No books found"
export const UserRequests = "User Requests"
export const AccesptButtonName = "Accept"
export const userDashboardtitle="User Dashboard"
export const MyProfileName="My Profile"
export const MybooksTitle="My Books"
export const LandingPageTitle="ONLINE OPEN BOOKS LIBRARY"
export const LoginTitle="Login"
export const SignupTitle="Sign Up"
export const LandingpageDes = "Books Library"
export const EmailName="Email"
export const EmailRequired="Email is Required"
export const PasswordName="Password"
export const PasswordErrorMsg="Password is Required"
export const GoogleLogin="Google Login"
export const SignupMsg="Don't have an account?"
export const Firstname="First Name"
export const FirstnameErrorMsg = "First Name is Required"
export const LastName="Last Name"
export const SecondnameErrorMsg = "Second Name is Required"
export const ConfirmPassword="Confirm Password"
export const ConfirmPasswordErrorMsg="Confirm Password is Required"
export const TermsAndCondtions = "I accept the terms & condtions"
export const SignupAccount = "Have an account?"
export const UpdateBookTitle="Update Book"
export const UpdateButton="Update"
export const ProfileUpdateTitle="Profile Update"
export const RejectName="Reject"

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
