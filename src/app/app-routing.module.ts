import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookComponent } from './addBook/add-book.component';
import { AdminDashboardComponent } from './adminDashboard/admin-dashboard.component';
import { AuthGuard } from './services/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { SingleBookInfoComponent } from './singleBookInfo/single-book-info.component';
import { UpdateBookComponent } from './updateBook/update-book.component';
import { UpdateProfileComponent } from './updateProfile/update-profile.component';


const routes: Routes = [
  {path:'',component:LandingpageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component:SignupComponent},
  {path: 'book/:id', component:SingleBookInfoComponent,canActivate:[AuthGuard]},
  {path:'user-dashboard',component:DashboardComponent,canActivate:[AuthGuard]},
  {path:'admin-dashboard',component:AdminDashboardComponent,canActivate:[AuthGuard]},
  {path:'add-book',component:AddBookComponent,canActivate:[AuthGuard]},
  {path:'update-profile/:id',component:UpdateProfileComponent,canActivate:[AuthGuard]},
  {path:'updatebook/:id',component:UpdateBookComponent,canActivate:[AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LandingpageComponent,SignupComponent,LoginComponent,
  DashboardComponent,AdminDashboardComponent,AddBookComponent,UpdateProfileComponent,UpdateBookComponent]
