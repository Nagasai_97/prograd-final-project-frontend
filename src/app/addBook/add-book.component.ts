import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common'
import { UserService } from '../services/user.service';
import { AdminService } from '../services/admin.service';
import ValidUrl from '../utils/ValidateURL';
import { AddBookTitle, Back, BookName, bookNameErrorMsg, BookTypeErrorMsg, BookTypeTitle, FileSize, ImageUrlErrorMsg, ImageUrlTitle, PdfErrorMsg, PdfTitle, SubmitButton } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  bookName:string
  pdfLink:string
  bookType:string
  status:string
  bookImg!:File
  fileList!: File;
  bookNameError:boolean
  pdfLinkError:boolean
  bookTypeError:boolean
  bookImgError:boolean
  isLoading:boolean
  fileSize:boolean
  back=Back
  title=AddBookTitle
  bookNameTtle=BookName
  pdfLinkTitle=PdfTitle
  imageUrlTitle=ImageUrlTitle
  bookTypeTitle=BookTypeTitle
  bookNameErrorMsg=bookNameErrorMsg
  pdfErrorMsg=PdfErrorMsg
  imageUrlErrorMsg=ImageUrlErrorMsg
  bookTypeErrorMsg=BookTypeErrorMsg
  submitButtontitle=SubmitButton
  fileTitle=FileSize


  constructor(private route:Router,private location: Location,private userService:UserService,private adminService:AdminService,private toster:ToastrService) {
    this.bookName = ''
    this.pdfLink = ''
    this.bookType=''
    this.status = ''
    this.bookNameError = false
    this.pdfLinkError = false
    this.bookTypeError=  false
    this.bookImgError = false
    this.isLoading = false
    this.fileSize = false
   }

  ngOnInit(): void {
  }
  async onSubmit(){
    if(localStorage.getItem('userType') == 'user'){
      this.status = 'inactive'
    }
    else {
      this.status = 'active'
    }
    
   if(this.bookName == ''&& this.bookType == ''){
    this.bookNameError = true
      this.pdfLinkError = true
      this.bookTypeError=  true
      this.bookImgError = true
  }
    else if(this.bookName == ''){
      this.bookNameError = true
    }
    else if(this.bookType =='' ){
   
      this.bookImgError = false
      this.bookTypeError = true
    }
    else if(this.fileList == undefined){
      this.bookImgError = false
      this.bookTypeError = false
      this.pdfLinkError = true
    }
    else if(this.bookName !== ''&& this.bookType !==''&&this.fileList !== undefined && !this.fileSize){
      this.isLoading = true
      this.bookNameError = false
      this.pdfLinkError = false
      this.bookTypeError=  false
      this.bookImgError = false
        const formData = new FormData();
        formData.append('pdf', this.fileList);
        const imageData = new FormData();
        imageData.append('pdf', this.bookImg);
         this.userService.uploadFile(formData).subscribe(res=>{
          this.pdfLink = res.Location
          this.userService.uploadFile(imageData).subscribe(res=>{
            this.bookImg = res.Location
          this.userService.addBook({bookname:this.bookName,
            pdfLink:this.pdfLink,
            type:this.bookType,bookImg:this.bookImg,
            userid:localStorage.getItem('id'),
            status:this.status}).subscribe(data=>{
              this.isLoading = false
              this.toTosterSucess()
              this.location.back()
          },
          error => {
            this.isLoading = false
            this.toTosterError()
            alert(error)
        });
        },err=>{
          this.isLoading = false
          console.log(err)
        }) 
      },err=>{
        this.isLoading = false
        console.log(err)
      }) 
  
  
  }
  }
  backbutton(){
    this.location.back()
  }
onFileSelected(event:any){
  this.fileSize = true
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.fileList = file;
    if(this.fileList.size < 5000000){
      this.fileSize = false
    }
    else {
      this.fileSize = true
    }
  }
}
onImageSelected(event:any){
  this.fileSize = true
  if (event.target.files.length > 0) {
    const file = event.target.files[0];
    this.bookImg = file;
    if(this.bookImg.size < 5000000){
      this.fileSize = false
    }
    else {
      this.fileSize = true
    }
  }
}
toTosterSucess(){
  this.toster.success("sucessfully Added")
}

toTosterError(){
  this.toster.error("Error")
}

}
