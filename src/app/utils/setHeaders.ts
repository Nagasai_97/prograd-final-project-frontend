import { HttpHeaders } from "@angular/common/http";

function setHeaders(access:any,refresh:any){
    const headerDist= new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*').
    set('Acess-Control-Allow-Methods','POST,GET')
    .set('accessToken', access)
    .set('refreshToken', refresh)

    return headerDist

}

export default setHeaders;