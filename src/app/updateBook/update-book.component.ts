import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router'
import { Location } from '@angular/common'
import ValidUrl from '../utils/ValidateURL';
import { ToastrService } from 'ngx-toastr';
import {  Back, BookName, bookNameErrorMsg, BookTypeErrorMsg, BookTypeTitle, FileSize, ImageUrlErrorMsg, ImageUrlTitle, PdfErrorMsg, PdfTitle, UpdateBookTitle, UpdateButton } from 'src/environments/environment';
@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {
  
id:any
bookName:string
  pdfLink:string
  bookType:string
  bookImg:string
  bookNameError:boolean
  pdfLinkError:boolean
  bookTypeError:boolean
  bookImgError:boolean
  fileList!: File;
  isPdfChange:boolean
  isLoading:boolean
  dataLoading:boolean
  fileSize:boolean
  back:string=Back
  title=UpdateBookTitle
  bookNameTtle=BookName
  pdfLinkTitle=PdfTitle
  imageUrlTitle=ImageUrlTitle
  bookTypeTitle=BookTypeTitle
  bookNameErrorMsg=bookNameErrorMsg
  pdfErrorMsg=PdfErrorMsg
  imageUrlErrorMsg=ImageUrlErrorMsg
  bookTypeErrorMsg=BookTypeErrorMsg
  updateButtontitle=UpdateButton
  fileTitle=FileSize
  
  constructor(private userService:UserService,private idRoute:ActivatedRoute,private location:Location,private toster:ToastrService) {
    this.bookName = ''
    this.pdfLink = ''
    this.bookType=''
    this.bookImg = ''
    this.bookNameError = false
    this.pdfLinkError = false
    this.bookTypeError=  false
    this.bookImgError = false
    this.isPdfChange = false
    this.isLoading=false
     this.fileSize=false
     this.dataLoading = true
   }

  ngOnInit(): void {
    this.dataLoading = true
    this.id = this.idRoute.snapshot.paramMap.get("id");
    this.userService.readBook(this.id).subscribe(data=>{
    this.bookName = data.bookname
    this.pdfLink = data.pdfLink
    this.bookType=data.type
    this.bookImg = data.bookImg
    this.dataLoading = false
    })
  }
  async onUpdate(){    
   if(this.bookName == ''&& this.bookType == ''&& this.bookImg == ''){
    this.bookNameError = true
      this.pdfLinkError = true
      this.bookTypeError=  true
      this.bookImgError = true
  }
    else if(this.bookName == ''){
      this.bookNameError = true
    }
    else if(this.bookImg == ''){
      this.bookNameError = false
      this.pdfLinkError = false
      this.bookImgError = true
      this.bookTypeError = true
    }
    else if(this.bookType =='' ){
      this.bookImgError = false
      this.bookTypeError = true
    }
    else if(this.bookName !== ''&& this.bookType !==''&& this.bookImg !== '' && !this.fileSize ){
      this.isLoading = true
      this.bookNameError = false
      this.pdfLinkError = false
      this.bookTypeError=  false
      this.bookImgError = false
      if(await ValidUrl(this.bookImg)){
        if(this.isPdfChange){
        const formData = new FormData();
        formData.append('pdf', this.fileList);
         this.userService.uploadFile(formData).subscribe(res=>{
          this.pdfLink = res.Location
          this.userService.updateBook(this.id,{bookname:this.bookName,
            pdfLink:this.pdfLink,
            type:this.bookType,bookImg:this.bookImg,
            userid:localStorage.getItem('id'),
            status:'active'}).subscribe(data=>{
              this.isLoading = false
              this.toTosterSucess()
              this.ngOnInit()
          },
          error => {
            this.toTosterError()
            alert(error)
        });
        },err=>{
          console.log(err)
        }) 
  
    }
    else {
      this.userService.updateBook(this.id,{bookname:this.bookName,
        pdfLink:this.pdfLink,
        type:this.bookType,bookImg:this.bookImg,
        userid:localStorage.getItem('id'),
        status:'active'}).subscribe(data=>{
          this.isLoading = false
          this.toTosterSucess()
          this.location.back()
      },
      error => {
        this.toTosterError()
        console.log(error)
    });
    }
  }
    else {
      alert("Invalid Image Url")
    }
  }
  }
  onFileSelected(event:any){
    if (event.target.files.length > 0) {
      this.isPdfChange = true
      const file = event.target.files[0];
      this.fileList = file;
      if(this.fileList.size < 5000000){
        this.fileSize = false
      }
      else {
        this.fileSize = true
      }
    }
    else {
      this.isPdfChange = false
    }
  }
  backbutton(){
    this.location.back()
  }

  toTosterSucess(){
    this.toster.success("Sucessfully Updateed")
    this.location.back()
  }
  toTosterError(){
    this.toster.error("Error")
  }

}
