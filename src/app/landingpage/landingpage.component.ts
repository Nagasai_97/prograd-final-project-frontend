import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'; 
import { HomeName, LandingpageDes, LandingPageTitle, LoginTitle, ReadButtonName, SignupTitle } from 'src/environments/environment';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.css']
})
export class LandingpageComponent implements OnInit {
  allBooks: any
  isLoading:boolean
  title=LandingPageTitle
  homeName=HomeName
  loginTitle=LoginTitle
  signUp=SignupTitle
  landingpageDes=LandingpageDes
  readButtonName=ReadButtonName

  constructor(private route:Router,private userService:UserService) {
this.isLoading = true
   }
  
  ngOnInit(): void {
    this.userService.landingPageAllBooks().subscribe(data=>{
      this.allBooks = data
      this.isLoading = false
    },
    error=>{
      console.log(error)
    })
  }
  onClickHome(){
    this.route.navigate(['/']);
  }
  onClickSignup(){
    this.route.navigate(['/signup']);
  }
  onClickLogin(){
    this.route.navigate(['/login']);
  }
  read(event:any){
    this.route.navigate([`book/${event.target.id}`])
  }

}
