import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { UserService } from '../services/user.service';
import { Location } from '@angular/common'
import { Back } from 'src/environments/environment';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';




@Component({
  selector: 'app-single-book-info',
  templateUrl: './single-book-info.component.html',
  styleUrls: ['./single-book-info.component.css']
  
})
export class SingleBookInfoComponent implements OnInit {
  id:any
  booktitle:string
  bookImg:string
  bookPdf:string 
 isLoading:boolean
 backName=Back
 faCoffee = faCoffee;
  
  constructor(private idRoute:ActivatedRoute,
    private route:Router,
    private userSerivce:UserService,
    private location:Location,
    private toster:ToastrService) {
    this.booktitle=""
    this.bookImg = ""
    this.bookPdf = ''
    this.isLoading = true
  
    
   }

  ngOnInit(): void {
    this.isLoading = true
      this.id = this.idRoute.snapshot.paramMap.get("id");
      this.userSerivce.readBook(this.id).subscribe(
        data=>{
          alert(data.status)
          this.booktitle=data.bookname
          this.bookImg = data.bookImg
          this.bookPdf=data.pdfLink
          this.isLoading = false
        },
        error=>{
          console.log(error)
        }
      )

  }
  backbutton(){
this.location.back()
  }
  toDownloadBook(){
    this.toster.info("sucessfully Downloaded")
  }

}
