import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Location } from '@angular/common'
import { UserService } from '../services/user.service';
import ValidateEmail from '../utils/validateEmail';
import ValidatePassword from '../utils/validatePassword';
import { ToastrService } from 'ngx-toastr';
import { ConfirmPassword, ConfirmPasswordErrorMsg,Back, EmailName, EmailRequired, Firstname, FirstnameErrorMsg, LastName, PasswordErrorMsg, PasswordName, ProfileUpdateTitle, SecondnameErrorMsg, UpdateButtonName, } from 'src/environments/environment';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
  id:any
  firstName: string;
  secondName:string
  email:string
  password:string
  confirmPassword:string
  isLoading:boolean
  firstNameError: boolean;
  secondNameError:boolean
  emailError:boolean
  passwordError:boolean
  confirmPasswordError:boolean
  title= ProfileUpdateTitle
  firstnameTitle=Firstname
  firstnameMsg = FirstnameErrorMsg
  lastnameTitle=LastName
  secondErrorMsg =SecondnameErrorMsg
  emailName=EmailName
  emailErrorMsg=EmailRequired
  passwordName=PasswordName
  passwordErrorMsg=PasswordErrorMsg
  confirmPasswordName=ConfirmPassword
  confirmPasswordErrorMsg=ConfirmPasswordErrorMsg
  updateButton=UpdateButtonName
  back=Back
 
  
  constructor(private idRoute:ActivatedRoute,
    private location:Location,
    private route:Router,
    private userService:UserService,
    private toster:ToastrService) { 
    this.firstName = ''
    this.secondName = ''
    this.email = ''
    this.password = ''
    this.confirmPassword = ''
    this.isLoading = true
    this.firstNameError = false
    this.secondNameError = false
    this.emailError = false
    this.passwordError = false
    this.confirmPasswordError = false
  }

  ngOnInit(): void {
    if(localStorage.getItem('userType') == "user"){
    this.id = this.idRoute.snapshot.paramMap.get("id");
    this.userService.getProfile(this.id).subscribe(data=>{
      this.firstName = data.firstname
    this.secondName = data.lastname
    this.email = data.email
    this.isLoading = false
    },
    err=>{
      console.log(err)
    })
    
  }
  else {
    this.route.navigate(['/user-dashboard'])
  }
}
  backbutton(){
this.location.back()
  }
  
  onUpdate(){
    if(this.firstName==''&& this.secondName==''&&this.email==''&&this.password==''&&this.confirmPassword==''){

      this.firstNameError = true
      this.secondNameError = true
      this.emailError = true
      this.passwordError = true
      this.confirmPasswordError = true
    }
    else if(this.firstName ==''){
      this.firstNameError = true;
    }
    else if(this.secondName == ''){
      this.secondNameError = true
    }
    else if(this.email == ''){
      this.emailError = true
    }
    else if(this.password == ''){
      this.passwordError = true
    }
    else if(this.confirmPassword == ''){
      this.passwordError = false
      this.confirmPasswordError = true;
    }
    else{
      if(this.password ===this.confirmPassword){
        if(ValidateEmail(this.email)){
          if(ValidatePassword(this.password)){
        this.userService.updateProfile({firstname:this.firstName,
          lastname:this.secondName,email:this.email,password:this.password},
          this.id).subscribe(data=>{
           this.toTosterSucess()
           this.backbutton()
          },
          err=>{
            this.toTosterError()
            console.log(err)
          })
        }
        else {
          alert("Invalid Password ")
        }
      }
        else {
          alert('Invalid Email')
        }
      }
      else {
        alert("Password and Confirm Password doest not match")
      }
    }


  }
  toTosterSucess(){
    this.toster.success("Sucessfully Updateed")
  }
  toTosterError(){
    this.toster.error("Error")
  }

}
