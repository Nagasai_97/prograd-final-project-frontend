import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'; 
import { ConfirmPassword, ConfirmPasswordErrorMsg, EmailName, EmailRequired, Firstname, FirstnameErrorMsg, LastName, LoginTitle, PasswordErrorMsg, PasswordName, SecondnameErrorMsg, SignupAccount, SignupTitle, TermsAndCondtions } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';
import ValidateEmail from '../utils/validateEmail';
import ValidatePassword from '../utils/validatePassword';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  firstName:string
  secondName:string
  email:string
  password:string
  confirmPassword:string
  validate:boolean
  checked:boolean
  isLoading:boolean
  userType:String
  firstNameError:boolean
  secondNameError:boolean
  emailError:boolean
  passwordError:boolean
  confirmPasswordError:boolean
  signUpTitle=SignupTitle
  firstnameTitle=Firstname
  firstnameMsg = FirstnameErrorMsg
  lastnameTitle=LastName
  secondErrorMsg =SecondnameErrorMsg
  emailName=EmailName
  emailErrorMsg=EmailRequired
  passwordName=PasswordName
  passwordErrorMsg=PasswordErrorMsg
  confirmPasswordName=ConfirmPassword
  econfirmPasswordErrorMsg=ConfirmPasswordErrorMsg
  termsAndCondtionsMsg=TermsAndCondtions
  signupAccount=SignupAccount
  login=LoginTitle



  constructor(private route:Router,private authService:AuthService) {
    this.firstName=''
    this.secondName=''
    this.email=''
    this.password=''
    this.confirmPassword=''
    this.validate=false;
    this.checked = false
    this.isLoading = true
    this.userType = ''
    this.firstNameError = false
    this.secondNameError = false
    this.emailError = false
    this.passwordError = false
    this.confirmPasswordError = false
   }
  ngOnInit(): void {
    if(localStorage.getItem("userType") == 'user'){
      this.route.navigate(['user-dashboard'])
    }
    else if(localStorage.getItem("userType") == 'admin'){
      this.route.navigate(['admin-dashboard'])
    }
    else {
      this.route.navigate(['signup'])
    }
  }

  onSubmitSignup(){
     if(this.firstName!==''&&this.secondName!==''&&this.email!==''&&this.password!==''&&this.confirmPassword!=='' && this.checked){
      this.firstNameError = false
      this.secondNameError = false
      this.emailError = false
      this.passwordError = false
      this.confirmPasswordError = false
      if(this.password ===this.confirmPassword){
        if(ValidateEmail(this.email)){
          if(ValidatePassword(this.password)){
        this.isLoading = false
        this.authService.setRegisterData({firstname:this.firstName,
          lastname:this.secondName,email:this.email,
          password:this.password})
         .subscribe(data=>{
          this.isLoading = true
           if(data.status == 200){
             this.route.navigate(['login'])
           }
           else {
            this.isLoading = true
             alert("Invalid Credentials")
           }
         },err=>{
           alert(err.error.data)
          this.isLoading = true
           console.log(err.error)
         })
        
        }
        else {
          alert("Invalid Password ")
        }
      }
        else {
          alert('Invalid Email')
        }
      }
      else {
        alert("Password and Confirm Password doest not match")
      }
    }
    else if(this.firstName==''&&this.secondName==''&&this.email==''&&this.password==''&&this.confirmPassword=='' ){
      this.firstNameError = true
      this.secondNameError = true
      this.emailError = true
      this.passwordError = true
      this.confirmPasswordError = true
    }
    else if(this.firstName ==''){
      this.firstNameError = true;
    }
    else if(this.secondName == ''){
      this.secondNameError = true
    }
    else if(this.email == ''){
      this.emailError = true
    }
    else if(this.password == ''){
      this.passwordError = true
    }
    else if(this.confirmPassword == ''){
      this.confirmPasswordError = true;
    }


  }

}
