import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { AccesptButtonName, AddBookTitle, AdminDashboardTitle, AllBooksTitle, BooksRequests, BooksTitle, deleteButtonname, EmptyListName, HomeName, Logout, ReadButtonName, RejectName, UpdateButtonName, UserRequests } from 'src/environments/environment';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  flag:boolean
  allBooks: any[]
  allRequests: any
  check:boolean
  ids:any
  isLoading:boolean
  search:string
  bookType:string
  title=AdminDashboardTitle
  addBookTitle=AddBookTitle
  logoutName = Logout
  homeName=HomeName
  allbooksTitle= AllBooksTitle
  booksRequests=BooksRequests
  booksTitle=BooksTitle
  readButtonname=ReadButtonName
  updateButtonName=UpdateButtonName
  deleteButtonname=deleteButtonname
  emptyName=EmptyListName
  userRequestsTitle=UserRequests
  accesptButtonName=AccesptButtonName
  rejectname=RejectName

  
  constructor(private route:Router,
    private adminService:AdminService,
    private cookiservice:CookieService,
    private toster:ToastrService) {
    this.flag = true
    this.check = false
    this.ids = []
    this.isLoading = true
    this.allBooks = []
    this.allRequests = []
    this.search = ''
    this.bookType=''

   }

  ngOnInit(): void {
    if(localStorage.getItem('userType') == "admin"){
    this.adminService.getAllBooks().subscribe(data=>{
      this.allBooks = data
      this.isLoading = false
    },
    error=>{
      console.log(error)
    })

  }
  else {
    this.route.navigate(['/user-dashboard'])
  }
}

  logout(){
    localStorage.removeItem("userType");
    localStorage.removeItem('id')
    this.cookiservice.deleteAll()
    this.route.navigate(['login'])
  }
  dashboard(){
    this.flag = true
    this.isLoading = true
        this.adminService.getAllBooks().subscribe(data=>{
          this.allBooks = data,
          this.isLoading = false
          this.bookType=''
            },
            error=>{
              console.log(error)
            })
 
  }
  request(){
    this.flag = false
    this.isLoading = true
    this.adminService.getAllRequests().subscribe(data=>{
      this.allRequests = data,
      this.isLoading = false
      this.bookType=''
        },
        error=>{
          console.log(error)
        })
  }
 
  read(event:any){
    this.route.navigate([`book/${event.target.id}`])
  }
  deleteBook(event:any){
    let id = event.target.id
    this.adminService.updateBook(id,{status:"delete"}).subscribe(data=>{
      this.isLoading = true
      this.adminService.getAllBooks().subscribe(data=>{
        this.isLoading = false
        this.allBooks = data
        this.isLoading = false
        this.toTosterDelete()
      },
      error=>{
        console.log(error)
      })
    })
    
  }
  updateBook(event:any){
    let id = event.target.id
      this.route.navigate([`updatebook/${id}`])
  }
  onchengeCheck(event:any){
    this.check = true
    let id=event.target.id
   let index = this.ids.indexOf(id)
  if(index == -1){
    this.ids.push(id)
  }
  else {
    this.ids.splice(index,1);
  }
  }
  acceptRequest(){
    this.ids.map((id:any)=>{
      this.adminService.acceptRequest(id,{status:'active'}).subscribe(data=>{
        this.ids = this.ids.filter(function(ele:any){ 
          return ele != id; 
      });
        this.flag = true
        this.isLoading = true
            this.adminService.getAllBooks().subscribe(data=>{
              this.allBooks = data,
              this.isLoading = false
              this.toAcceptRequest()
    },
    error=>{
      console.log(error)
    })
    },
    error=>{
      console.log(error)
    })

    })
    
  }
  deleteRequest(){
    this.ids.map((id:any)=>{
      this.adminService.acceptRequest(id,{status:"delete"}).subscribe(data=>{
        this.ids = this.ids.filter(function(ele:any){ 
          return ele != id; 
      });
        this.flag = false
        this.isLoading = true
        this.adminService.getAllRequests().subscribe(data=>{
          this.allRequests = data,
          this.isLoading = false
          this.toDeleteRequest()
            },
            error=>{
              console.log(error)
            })
    })

    })
  }
  addBook(){
    this.route.navigate(["add-book"])
  }
  home(){
    this.flag = true
  }
  toAcceptRequest(){
    this.toster.success("Sucessfully Accepted")
  }
  toDeleteRequest(){
    this.toster.error("Request Deleted")
  }
  toTosterDelete(){
    this.toster.error("Sucessfully Deleted")
  }
  toEmptySearch(){
    this.toster.error("Search value is required")
  }

  onChangeSearch(event:any){
    let search = event.target.value
    if(search){
      this.isLoading = true
    this.adminService.getSearch(search).subscribe(
      res=>{
        this.isLoading = false
        this.allBooks = res
      },
      error=>{
        console.log(error)
      })
    }
    else {
        this.toEmptySearch()
        this.dashboard()
  }
}
typeOfBook(){
  if(this.bookType){
    this.flag = true
    this.isLoading = true
    this.adminService.getFilter(this.bookType).subscribe(data=>{
      this.isLoading = false
      this.allBooks = data;
    },
    error=>{
      console.log(error)
    })
}
else {
  this.dashboard()
}
}
}
