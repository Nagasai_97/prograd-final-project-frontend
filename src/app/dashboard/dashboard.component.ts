import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { AddBookTitle, AllBooksTitle, deleteButtonname, EmptyListName, HomeName, Logout, MybooksTitle, MyProfileName, ReadButtonName, UpdateButtonName, userDashboardtitle } from 'src/environments/environment';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  navFlag:boolean
  userId:any
  userTotalBooks:any
  isLoading:boolean
  allbooks:any
  bookType:string
  search:string
  profileName:string
  title=userDashboardtitle
  homeName=HomeName
  logoutname=Logout
  myProfilename=MyProfileName
  addBookName=AddBookTitle
  allBooksTitle=AllBooksTitle
  myBooksTitle=MybooksTitle
  emprtListName=EmptyListName
  readButtonname=ReadButtonName
  updateButtonName=UpdateButtonName
  deleteButtonname=deleteButtonname

  constructor(private route:Router,
    private userService:UserService
    ,private cookiservice:CookieService,
    private toster:ToastrService) { 
    this.navFlag = true
    this.userId = localStorage.getItem('id')
    this.isLoading = true
    this.allbooks = []
    this.bookType=''
    this.search = ''
    this.profileName = ''
    
  }

  ngOnInit(): void {
    this.isLoading = true
    if(localStorage.getItem('userType') == "user"){
    this.userService.getAllBooks()
       .subscribe(data=>{
        this.allbooks = data
         this.userService.getProfile(this.userId).
        subscribe(data=>{
          this.profileName = data.firstname[0].toUpperCase()
          this.isLoading =false
         },
         error=>{
           console.log(error)
         })
        
       },
       error=>{
         console.log(error)
       })
  }
  else {
    this.route.navigate(['/admin-dashboard'])
  }
}
  logout(){
    localStorage.removeItem("userType");
    localStorage.removeItem('id')
    this.cookiservice.deleteAll()
    this.route.navigate(['login'])
  }
  profileUpdate(){
    this.route.navigate([`update-profile/${this.userId}`])
  }
  addBook(){
    this.route.navigate(['add-book'])
  }

  myBooks(){
    this.search = ''
    this.bookType=''
    this.navFlag = false
    this.isLoading = true
     this.userService.getUserAllBooks(this.userId).subscribe(data=>{
      this.userTotalBooks = data,
      this.isLoading = false
     },
     error=>{
       console.log(error)
     }
     ) 
  }
  getallBooks(){
    this.bookType=''
    this.search = ''
    this.navFlag = true
    this.userService.getAllBooks()
       .subscribe(data=>{
         this.isLoading =false
         this.allbooks= data
       },
       error=>{
         console.log(error)
       })
  }
  read(event:any){
    let id = event.target.id
    this.route.navigate([`book/${id}`])
  }
  update(event:any){
    let id = event.target.id
      this.route.navigate([`updatebook/${id}`])
    
  }
  home(){
    this.navFlag = true
    this.userService.getAllBooks()
       .subscribe(data=>{
         this.isLoading =false
         this.allbooks= data
       },
       error=>{
         console.log(error)
       })
  }
  typeOfBook(){
    if(this.navFlag && this.bookType){
    this.isLoading = true
    this.userService.getFilter(this.bookType).subscribe(data=>{
      this.isLoading = false
      this.allbooks = data;
    },
    error=>{
      console.log(error)
    })
  }
  else if(!this.navFlag && this.bookType){
    this.isLoading = true
    let id = localStorage.getItem('id')
    this.userService.getFilterByUser(this.bookType,id).subscribe(data=>{
      this.isLoading = false
      this.userTotalBooks = data;
    },
    error=>{
      console.log(error)
    })
  }
  else {
    this.getallBooks()
  }
  }
  delete(event:any){
    let id = event.target.id
    this.userService.updateBook(id,{status:"deleted"}).subscribe(
      data=>{
        this.navFlag = false
        this.isLoading = true
         this.userService.getUserAllBooks(this.userId).subscribe(data=>{
          this.userTotalBooks = data,
          this.toTosterDelete()
          this.isLoading = false
         },
         error=>{
           console.log(error)
         }
         ) 
      }
    )
    
  }
  toTosterDelete(){
    this.toster.error("Sucessfully Deleted")
  }
  toEmptySearch(){
    this.toster.error("Search value is required")
  }
  onChangeSearch(event:any){
    this.bookType = ''
    let search = event.target.value
    if(search){
      this.isLoading = true
    this.userService.getSearch(search).subscribe(
      res=>{
        this.isLoading = false
        this.allbooks = res
      },
      error=>{
        console.log(error)
      })
  
  }
       else {
        this.toEmptySearch()
        this.getallBooks()
        }
}


}
