import { Component, Injectable, OnInit } from '@angular/core';
import {Router} from '@angular/router'; 
import { SocialAuthService, GoogleLoginProvider } from "angularx-social-login";
import { CookieService } from 'ngx-cookie-service';
import { EmailName, EmailRequired, GoogleLogin, LoginTitle, PasswordErrorMsg, PasswordName, SignupMsg, SignupTitle } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';
import ValidateEmail from '../utils/validateEmail'
import ValidatePassword from '../utils/validatePassword';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  email:string
  password:string
  isLoading:boolean
  emailError:boolean
  passwordError:boolean
  loginTitle=LoginTitle
  emailName=EmailName
  emailErrorMsg=EmailRequired
  passwordName=PasswordName
  passwordErrorMsg=PasswordErrorMsg
  googleLogin=GoogleLogin
  signUpMsg=SignupMsg
  signUp=SignupTitle

  constructor(private route:Router,
    private socialAuthservice:SocialAuthService,
    private authService:AuthService,
    private cookieService:CookieService) { 
  this.isLoading = false
  this.email = ''
  this.password = ''
  this.emailError= false
  this.passwordError=false
  }

  ngOnInit(): void {
    if(localStorage.getItem("userType") == 'user'){
      this.route.navigate(['user-dashboard'])
    }
    else if(localStorage.getItem("userType") == 'admin'){
      this.route.navigate(['admin-dashboard'])
    }
    else {
      this.route.navigate(['login'])
    }
  }
   onClickLogin(){
     if(this.email == '' && this.password == ''){
      this.emailError= true
      this.passwordError=true
     }
     else if(this.email == ''){
      this.emailError= true
     }
     else if(this.password == ''){
      this.passwordError=true
     }
     else {
      this.emailError= false
      this.passwordError=false
     if(ValidateEmail(this.email)) {
      if(ValidatePassword(this.password))
      {
      this.isLoading = true
      this.authService.setLoginData({email:this.email,
        password:this.password})
       .subscribe(res=>{
        this.isLoading = false
         localStorage.setItem('userType',res.type)
         localStorage.setItem('token',res.accesstoken)
         localStorage.setItem('id',res.id)
         if(localStorage.getItem('userType')=== 'user'){
          this.cookieService.set('accessToken',res.accesstoken)
          this.cookieService.set('refreshToken',res.refreshToken)
           this.route.navigate(['user-dashboard'])
         }
         else if(localStorage.getItem('userType')=== 'admin'){
           this.cookieService.set('accessToken',res.accesstoken)
           this.cookieService.set('refreshToken',res.refreshToken)
           this.route.navigate(['admin-dashboard'])
         }
         else {
          this.isLoading = false
           this.route.navigate(['login'])
         }
       },(error)=>{
         this.isLoading = false
         alert('Invalid Credentials')
       })
    }
    else {
      alert('Invalid Password')
    }
  }
  else {
    alert('Invalid Email')
  }
}
   
    
  }
 
 
   onClickGoogle():void{
    this.socialAuthservice.signIn(GoogleLoginProvider.PROVIDER_ID).then((res)=>{
      console.log(res)
      let {firstName,lastName,email} =  res
      console.log(firstName,lastName,email)
    this.authService.setRegisterData({firstname:firstName,
      lastname:lastName,email:email,
      password:''}).subscribe(res=>{
        localStorage.setItem('userType',res.type)
         localStorage.setItem('id',res.id)
         if(localStorage.getItem('userType')=== 'user'){
          this.cookieService.set('accessToken',res.accesstoken)
          this.cookieService.set('refreshToken',res.refreshToken)
           this.route.navigate(['user-dashboard'])
         }
         else if(localStorage.getItem('userType')=== 'admin'){
           this.cookieService.set('accessToken',res.accesstoken)
           this.cookieService.set('refreshToken',res.refreshToken)
           this.route.navigate(['admin-dashboard'])
         }
         else {
          this.isLoading = false
           this.route.navigate(['login'])
         }

    },error=>{
      alert(error)
    })
    })
    
   }

   signOut(): void {
    this.socialAuthservice.signOut();
  }
  }

