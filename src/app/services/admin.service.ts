import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { BaseURL } from 'src/environments/environment';
import setHeaders from '../utils/setHeaders';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  headerDist:any

  constructor(private http: HttpClient,private cookieService:CookieService) { 
     this.headerDist = setHeaders(this.cookieService.get('accessToken'),this.cookieService.get('refreshToken'))
  }
  
  getAllBooks():Observable<any>{
    
    return this.http.get(`${BaseURL}allbooks`,{headers:this.headerDist})
  }
  getAllRequests():Observable<any>{
    return this.http.get(`${BaseURL}allrequests`,{headers:this.headerDist})
  }
 
  deleteBook(id:any):Observable<any>{
    return this.http.delete(`${BaseURL}deleteBook/${id}`,{headers:this.headerDist})
  
  }
  acceptRequest(id:any,data:any):Observable<any>{
    return this.http.put(`${BaseURL}updateBook/${id}`,data,{headers:this.headerDist})
  }
  deleteRequest(id:any):Observable<any>{
    return this.http.delete(`${BaseURL}deleteBook/${id}`,{headers:this.headerDist})
  
  }
  addBook(bookData:any):Observable<any>{
    return this.http.post(`${BaseURL}addbook`,bookData,{headers:this.headerDist})
  }
  getFilter(type:any):Observable<any>{
    return this.http.get(`${BaseURL}filter/${type}`,{headers:this.headerDist})
  }
  getSearch(search:any):Observable<any>{
    return this.http.get(`${BaseURL}search/${search}`,{headers:this.headerDist})
  }
  updateBook(id:any,data:any):Observable<any>{
    return this.http.put(`${BaseURL}updateBook/${id}`,data,{headers:this.headerDist})
  }
}
