import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseURL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
  export class AuthService {
    constructor(private http: HttpClient) {
    }
    setLoginData(loginData:{}):Observable<any>{
      const headerDist= new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*').
      set('Acess-Control-Allow-Methods','POST');
      return this.http.post(`${BaseURL}login`,loginData,{headers:headerDist})
    }

     setRegisterData(signUpData:{}):Observable<any> {
        const headerDist= new HttpHeaders()
            .set('content-type', 'application/json')
            .set('Access-Control-Allow-Origin', '*').  set('Acess-Control-Allow-Methods','POST');
            return this.http.post(`${BaseURL}signup`,signUpData,{headers:headerDist})
    }
    loggedIn(){
      return !!localStorage.getItem('token')
    }


}