import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { BaseURL } from 'src/environments/environment'; // name
import setHeaders from '../utils/setHeaders';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headerDist:any
  constructor(private http: HttpClient,private cookieService:CookieService) {
    this.headerDist = setHeaders(this.cookieService.get('accessToken'),this.cookieService.get('refreshToken'))
   }

  landingPageAllBooks():Observable<any>{
     return this.http.get(`${BaseURL}landingpage`,{headers:this.headerDist})
  }
  getAllBooks():Observable<any>{
     return this.http.get(`${BaseURL}allbooks`,{headers:this.headerDist})
  }
  getUserAllBooks(id:any):Observable<any>{
    return this.http.get(`${BaseURL}mybooks/${id}`,{headers:this.headerDist})
  }
  updateBook(id:any,data:any):Observable<any>{
    return this.http.put(`${BaseURL}updateBook/${id}`,data,{headers:this.headerDist})
  }
  deleteBook(id:any):Observable<any>{
    return this.http.delete(`${BaseURL}deleteBook/${id}`,{headers:this.headerDist})
  }
  readBook(id:any):Observable<any>{
    return this.http.get(`${BaseURL}readBook/${id}`,{headers:this.headerDist})
  }
  addBook(bookData:any):Observable<any>{
    return this.http.post(`${BaseURL}addbook`,bookData,{headers:this.headerDist})
  }
  updateProfile(profileData:any,id:any):Observable<any>{
    return this.http.put(`${BaseURL}updateprofile/${id}`,profileData,{headers:this.headerDist})
  }
  getProfile(id:any):Observable<any>{
    return this.http.get(`${BaseURL}getProfile/${id}`,{headers:this.headerDist})
  }
  getFilter(type:any):Observable<any>{
    return this.http.get(`${BaseURL}filter/${type}`,{headers:this.headerDist})
  }
  getFilterByUser(type:any,id:any):Observable<any>{
    return this.http.get(`${BaseURL}filterByUser/${type}/${id}`,{headers:this.headerDist})
  }
  uploadFile(file:any):Observable<any>{
    return this.http.post(`${BaseURL}upload`,file)
  }
  getSearch(search:any):Observable<any>{
    return this.http.get(`${BaseURL}search/${search}`,{headers:this.headerDist})
  }
  
}
