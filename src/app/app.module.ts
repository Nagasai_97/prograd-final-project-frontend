import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule,routingComponents } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { SocialLoginModule, SocialAuthServiceConfig,GoogleLoginProvider } from 'angularx-social-login';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { AdminService } from './services/admin.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {CookieService} from 'ngx-cookie-service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SingleBookInfoComponent } from './singleBookInfo/single-book-info.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ToastrModule } from 'ngx-toastr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { AuthGuard } from './services/auth.guard';



const google_clint_id:string ="630046114787-1hhbvvqv5lu52j0f5v805a681njfbevk.apps.googleusercontent.com"; 

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    SingleBookInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ShareButtonsModule,
  ShareIconsModule,
  SocialLoginModule,
  BrowserAnimationsModule,
  HttpClientModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  PdfViewerModule,
  NgxExtendedPdfViewerModule,
  ToastrModule.forRoot(),
  FontAwesomeModule,
  MatMenuModule,
  MatButtonModule,
  
  
  ],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(google_clint_id)
        }
      ],
      AuthService,
      UserService,
      AdminService,
      CookieService,
      AuthGuard
      
    } as SocialAuthServiceConfig,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
